from numpy.core.function_base import linspace
import pandas as pd
import npgamma
from pymedphys import gamma
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from scipy import interpolate
from matplotlib import rc
rc('mathtext', default='regular')

class GammaIndexPlot:
    def __init__(self) -> None:
        self.data_short = None
        self.data_append = None
        self.data_long = None
        self.data_diff = None
        self.coords = None
        self.reference = None
        self.evaluation = None
        self.gamma_index = None
        self.gamma_result = None
        self.dose_criteria = 3.0  # %
        self.distance_criteria = 3.0  # mm
        self.fn = ""
        self.pos_range = (0, 30)
        self.fontsize=20
        self.dpi=100
        self.font2 = {
        'weight' : 'normal',
        'size'   : self.fontsize,
        }

    def plot_passing_rates(self, passing_rates):
        # figsize=(80,80),dpi=self.dpi
        f = plt.figure()
        ax1 = plt.gca()

        xx, yy = np.meshgrid(self.distance_criteria, self.dose_criteria, )
        im1 = ax1.pcolormesh(xx, yy, passing_rates, shading='nearest')

        co1 = ax1.contour(self.distance_criteria, self.dose_criteria,
                          passing_rates, colors='k', linewidth=1)
        zc = co1.collections
        plt.setp(zc, linewidth=1)
        plt.clabel(co1, fontsize=10, fmt='%1.0f')

        co2 = ax1.contour(self.distance_criteria, self.dose_criteria,
                          passing_rates, colors='#f70c0c', levels=[95])
        zc = co2.collections
        plt.setp(zc, linewidth=2)
        plt.clabel(co2, fontsize=12, fmt='%1.0f')

        ax1.set_title('Gamma Pass Rates')
        ax1.set_xlabel('Distance to agreement (mm)',self.font2)
        ax1.set_ylabel('Dose difference (%)',self.font2)
        c1 = plt.colorbar(im1, ax=ax1)
        c1.set_label('Pass rate (%)', rotation=270, labelpad=12)

    def calc_gamma(self):

        lower_percent_dose_cutoff = 0
        local = False
        # self.dose_criteria = np.arange(0.1, 3.01, 0.01)  # %
        self.dose_criteria = 3.0  # %
        # self.distance_criteria = np.arange(0.1, 3.01, 0.01)  # mm
        self.distance_criteria = 3.0  # mm

        pymedphys_gamma_options = {
            'dose_percent_threshold': self.dose_criteria,
            'distance_mm_threshold': self.distance_criteria,
            'lower_percent_dose_cutoff': lower_percent_dose_cutoff,
            'interp_fraction': 20,  # Should be 10 or more for more accurate results
            'max_gamma': 4,
            'random_subset': None,
            'local_gamma': local,
            'ram_available': 2**30,  # 1/2 GB
            'quiet': False
        }

        self.coords = self.data_short.iloc[:, 0]
        coords = (self.coords,)
        self.reference = self.data_short.iloc[:, 1]
        self.evaluation = self.data_long.iloc[:, 1]
        print(len(coords))
        # self.gamma = np.round(npgamma.calc_gamma(
        #     self.coords, self.reference,
        #     self.coords, self.evaluation,
        #     0.3, 0.03, num_threads=4), decimals=3)

        pymedphys_gamma = gamma(
            coords, self.reference,
            coords, self.evaluation,
            **pymedphys_gamma_options)
        self.gamma_index = pymedphys_gamma
        print(pymedphys_gamma)
        print(len(pymedphys_gamma))
        self.gamma_result = self.data_short.copy()
        self.gamma_result.iloc[:, 0] = self.coords
        self.gamma_result.iloc[:, 1] = self.gamma_index

        # pymedphys_pass_rates = np.zeros_like(pass_rates)
        # for item in pymedphys_gamma:
        #     i = list(self.dose_criteria).item[0]
        #     # j = list(self.distance_criteria).index(key[1])
        #     print(i,j)

        #     valid_gamma = item[~np.isnan(item)]
        #     passing = 100 * np.sum(valid_gamma <= 1) / len(valid_gamma)
        #     # pymedphys_pass_rates[i, j] = passing

    def plot_gamma1(self):

        fig = plt.figure(figsize=(20, 20),dpi=self.dpi)
        ax1 = fig.add_subplot(111)

        # pandas plot, ref https://www.huaweicloud.com/articles/e6e2c70b3c44fb05be461c58b7465483.html
        lns1 = self.data_short.plot(
            x=0, y=1, style='r-.', label='calculated', ax=ax1)
        lns2 = self.data_long.plot(
            x=3, y=4, style='k-', label='Measured', ax=ax1)

        # double y, ref https://blog.csdn.net/autoliuweijie/article/details/51594373 and https://pandas.pydata.org/pandas-docs/version/0.9.1/visualization.html
        ax2 = ax1.twinx()
        lns3 = self.data_diff.plot(
            x=0, y=1, style='b--', label='Difference', ax=ax2)

        # draw a horizon line, ref https://blog.csdn.net/u_7890/article/details/92801768
        lns4 = ax2.axhline(y=0, color='k', linestyle='-')

        ax1.tick_params(labelsize=self.fontsize)
        ax2.tick_params(labelsize=self.fontsize)
        
        lines, labels = ax1.get_legend_handles_labels()
        lines2, labels2 = ax2.get_legend_handles_labels()
        ax1.legend(prop = {'size':self.fontsize})
        ax2.legend(lines + lines2, labels + labels2, loc=0,prop = {'size':self.fontsize})
        # combine legend, ref https://www.cnblogs.com/atanisi/p/8530693.html
        # fig.legend(loc=1, bbox_to_anchor=(1, 1), bbox_transform=ax1.transAxes,prop = {'size':self.fontsize})
        # lns = lns1+lns2+lns3+lns4
        # labs = [l.get_label() for l in lns]
        # ax1.legend(lns, labs, loc=0)
        # ax2.get_legend().remove()
        # rm biankuang, ref https://blog.csdn.net/Caiqiudan/article/details/109647295
        # ax1.spines['right'].set_visible(False)
        ax1.spines['top'].set_visible(False)
        # ax1.spines['right'].set_visible(False)
        ax2.spines['top'].set_visible(False)

        # set xlim, ref https://stackoverflow.com/questions/27425015/python-pandas-timeseries-plots-how-to-set-xlim-and-xticks-outside-ts-plot
        ax1.set_xlim(self.pos_range)
        ax1.set_ylim((0, 100))
        ax2.set_ylim((-20, 20))
        ax1.set_xlabel("Position(cm)",self.font2)
        ax1.set_ylabel("Dose(%)",self.font2)
        ax2.set_ylabel("Difference(%)",self.font2)

        # save, https://www.cnblogs.com/bymo/p/7286415.html
        fig.savefig("pic/"+self.fn+"_"+'fig_long.png')

    def plot_gamma2(self):
        # set fig size, ref, https://blog.csdn.net/cdqn10086/article/details/72403899
        fig = plt.figure(figsize=(16, 4),dpi=self.dpi)
        ax1 = fig.add_subplot(111)
        ax1.tick_params(labelsize=self.fontsize)

        lns1 = self.gamma_result.plot(
            x=0, y=1, style='g-', label='gamma', ax=ax1)
        lns4 = ax1.axhline(y=1, color='r', linestyle='-')
        # rm legend, ref, https://www.delftstack.com/zh/howto/matplotlib/how-to-remove-legend-from-a-figure-in-matplotlib/
        ax1.get_legend().remove()
        ax1.set_xlim(self.pos_range)
        ax1.set_ylim((0, 1.2))
        ax1.set_xlabel("Position(cm)",self.font2)
        ax1.set_ylabel("Gamma Index",self.font2)
        ax1.spines['top'].set_visible(False)
        ax1.spines['right'].set_visible(False)

        fig.savefig("pic/"+self.fn+"_"+'gamma_index.png')

    def plot_gamma3(self):
        # ref, https://nbviewer.jupyter.org/github/SimonBiggs/npgamma/blob/master/Module%20usage%203D.ipynb
        fig = plt.figure(figsize=(16, 4),dpi=self.dpi)
        ax1 = fig.add_subplot(111)
        ax1.tick_params(labelsize=self.fontsize)

        valid_gamma = self.gamma_index[~np.isnan(self.gamma_index)]
        valid_gamma[valid_gamma > 4] = 4
        # passrate=np.sum(valid_gamma <= 1) / len(valid_gamma)
        # passrate, ref, https://github.com/pymedphys/pymedphys/blob/cb34c992de8d442eced3385018a194364060092d/examples/drafts/gamma/compare_to_flash_gamma.ipynb
        passing = 100 * np.sum(valid_gamma <= 1) / len(valid_gamma)
        valid_gamma_cum = []
        for i in np.arange(0.0, 4, 0.1):
            vec = [subdata for subdata in valid_gamma if (
                subdata <= i+1 and subdata > i)]
            sub_valid_gamma_cum = 0
            if len(vec) > 0:
                max_value = i
                print(vec)
                sub_valid_gamma_cum = 100 * len(vec) / len(valid_gamma)
            valid_gamma_cum.append(sub_valid_gamma_cum)

        # ref, https://stackoverflow.com/questions/45888179/why-doesnt-the-normed-parameter-for-matplotlib-histograms-do-anything
        # https://zhuanlan.zhihu.com/p/163435958
        # however ,no normd, so use density
        # bins=np.arange(0,4,24)
        # valid_gamma=self.gamma_result.iloc[:,1].value_counts(normalize=True)
        # print(valid_gamma)
        # score_cut = pd.cut(valid_gamma,bins = bins.tolist())
        # print(pd.value_counts(score_cut,sort=False))
        # hist, bins, patches = plt.hist(valid_gamma_cum, bins=40,density=True,stacked=True)
        # hist, bins, patches = plt.hist(valid_gamma_cum, bins=40)
        x = np.arange(0.0, 4, 0.1)
        y = valid_gamma_cum
        # plt.bar(x,y)
        xmin = 0.0
        xmax = 4.0
        nbin = 40
        # n, bin, patches = plt.hist(valid_gamma,bins=nbin)

        # ref, https://www.cnblogs.com/hope100/p/4045837.html
        bin, patches = np.histogram(valid_gamma, bins=nbin, range=(xmin, xmax))
        # plt.close()
        # plot_hist=n/nbin
        bin = bin/float(len(valid_gamma))*100
        ax1.bar(patches[:-1], bin,width=.1)
        # plt.hold
        # ax2 = ax1.twinx()
        # ax2 = ax1.twiny()
        
        # ax1.plot(self.data_append[:][0], self.data_append[:]
        #          [1], color='r', linestyle='-')

        # hist, bins, patches = plt.hist(valid_gamma,bins=40, density=True,stacked=True)
        # print(len(hist),hist,bins)
        # plt.plot(bins[0:-1],hist/sum(hist),'')
        # self.gamma_result.iloc[:,1].plot.hist(grid=True, bins=40, rwidth=0.9,
        #    color='#607c8e')

        plt.xlim([0, 4])
        # plt.ylim([0,100])
        print(passing)

        # ax1.set_xlim((0, 30))
        # ax1.set_ylim((0, 1.2))
        ax1.set_xlabel("Gamma Index",self.font2)
        ax1.set_ylabel("Percentage of points",self.font2)
        ax1.spines['top'].set_visible(False)
        ax1.spines['right'].set_visible(False)
        # ax2.spines['top'].set_visible(False)
        # ax2.spines['right'].set_visible(False)
        

        fig.savefig("pic/"+self.fn+"_"+'gamma_passrate.png')
    
    def plot_gamma4(self):
        # ref, https://nbviewer.jupyter.org/github/SimonBiggs/npgamma/blob/master/Module%20usage%203D.ipynb
        fig = plt.figure(figsize=(16, 4),dpi=self.dpi)
        ax1 = fig.add_subplot(111)
        ax1.tick_params(labelsize=self.fontsize)
        xdata=self.data_append[:][0].values.tolist()
        ydata=self.data_append[:][1].values.tolist()
        # xdata=self.data_append.iloc[:,0]
        # ydata=self.data_append.iloc[:,1]
        print(xdata,ydata)
        ax1.bar(x=xdata, height=ydata, color='b',width=.1)
        # self.data_append.plot(kind='bar',x=0, y=1,ax=ax1,width=.8)
        plt.tight_layout()
        # self.data_append.bar(x=0, y=1,ax=ax1)
        # plt.xlim()
        ax1.set_xlim((0, 4))
        ax1.set_ylim((0, 40))
        # plt.ylim([0,100])
        # ax1.set_xlim((0, 30))
        # ax1.set_ylim((0, 1.2))
        ax1.set_xlabel("Gamma Index",self.font2)
        ax1.set_ylabel("Percentage of points",self.font2)
        ax1.spines['top'].set_visible(False)
        ax1.spines['right'].set_visible(False)
        # ax2.spines['top'].set_visible(False)
        # ax2.spines['right'].set_visible(False)
        fig.savefig("pic/"+self.fn+"_"+'gamma_passrate_fake.png')

    def plot_togethor(self):

        fig = plt.figure()
        ax1 = fig.add_subplot(311)

        # pandas plot, ref https://www.huaweicloud.com/articles/e6e2c70b3c44fb05be461c58b7465483.html
        lns1 = self.data_short.plot(
            x=0, y=1, style='r-', label='calculated', ax=ax1)
        lns2 = self.data_long.plot(
            x=3, y=4, style='k-', label='Measued', ax=ax1)

        # double y, ref https://blog.csdn.net/autoliuweijie/article/details/51594373 and https://pandas.pydata.org/pandas-docs/version/0.9.1/visualization.html
        ax2 = ax1.twinx()
        lns3 = self.data_diff.plot(
            x=0, y=1, style='b--', label='Difference', ax=ax2)

        # draw a horizon line, ref https://blog.csdn.net/u_7890/article/details/92801768
        lns4 = ax2.axhline(y=50, color='k', linestyle='-')

        # combine legend, ref https://www.cnblogs.com/atanisi/p/8530693.html
        fig.legend(loc=1, bbox_to_anchor=(1, 1), bbox_transform=ax1.transAxes)

        # rm biankuang, ref https://blog.csdn.net/Caiqiudan/article/details/109647295
        # ax1.spines['right'].set_visible(False)
        ax1.spines['top'].set_visible(False)
        # ax1.spines['right'].set_visible(False)
        ax2.spines['top'].set_visible(False)

        # set xlim, ref https://stackoverflow.com/questions/27425015/python-pandas-timeseries-plots-how-to-set-xlim-and-xticks-outside-ts-plot
        ax1.set_xlim(self.pos_range)
        ax1.set_ylim((0, 100))
        ax2.set_ylim((0, 100))
        ax1.set_xlabel("Position(cm)",self.font2)
        ax1.set_ylabel("Dose(%)",self.font2)
        ax2.set_ylabel("Difference(%)",self.font2)

        ax3 = fig.add_subplot(312)

        lns1 = self.gamma_result.plot(
            x=0, y=1, style='g-', label='gamma', ax=ax3)
        lns4 = ax1.axhline(y=1, color='r', linestyle='-')
        ax3.set_xlim(self.pos_range)
        ax3.set_ylim((0, 1.2))
        ax3.set_xlabel("Position(cm)",self.font2)
        ax3.set_ylabel("Gamma Index",self.font2)
        ax3.spines['top'].set_visible(False)
        ax3.spines['right'].set_visible(False)

        ax4 = fig.add_subplot(313)

        lns1 = self.gamma_result.plot(
            x=0, y=1, style='g-', label='gamma', ax=ax4)
        lns4 = ax1.axhline(y=1, color='r', linestyle='-')
        ax4.set_xlim(self.pos_range)
        ax4.set_ylim((0, 1.2))
        ax4.set_xlabel("Position(cm)",self.font2)
        ax4.set_ylabel("Gamma Index",self.font2)
        ax4.spines['top'].set_visible(False)
        ax4.spines['right'].set_visible(False)

        # save, https://www.cnblogs.com/bymo/p/7286415.html
        fig.savefig("pic/"+self.fn+"_"+'fig_all.png')

    def parse_exel(self, fn_exel):
        self.fn = fn_exel.split(".")[0]
        data = pd.read_excel(fn_exel, header=None)
        print(data)
        # howto select data,
        # https://blog.csdn.net/kengmila9393/article/details/81082161
        # https://www.cnblogs.com/nxf-rabbit75/p/10105271.html

        self.data_short = data.iloc[:, 0:2]
        self.data_long = data.iloc[:, 3:5]
        print(self.data_short)
        print(self.data_long)

    def parse_exel2(self, fn_exel):
        # self.fn = fn_exel.split(".")[0]
        data = pd.read_excel(fn_exel, header=None)
        print(data)
        # howto select data,
        # https://blog.csdn.net/kengmila9393/article/details/81082161
        # https://www.cnblogs.com/nxf-rabbit75/p/10105271.html

        self.data_append = data.iloc[:, 0:2]
        self.data_append[:][1] *= 100

    def myinterp(self):
        data1 = self.data_short.dropna().iloc
        x = data1[:][0]
        y = data1[:][1]
        print(type(x))
        print(type(y))
        print(type(data1))
        print(type(self.data_short))
        data2 = self.data_long.iloc
        xx = data2[:, 0]
        f = interpolate.interp1d(x, y, kind='linear', bounds_error=False)
        yy = f(xx)
        # print(self.data_short.iloc[174, 1])
        print(self.data_short.dropna())
        self.data_short.iloc[:, 0] = xx
        self.data_short.iloc[:, 1] = yy
        print(self.data_short)

    def nomalization(self):
        self.norm2max(self.data_short)
        self.norm2max(self.data_long)

    def norm2max(self, data_in):
        data = data_in.iloc[:, 1]
        data = data/(data.max())*100
        print(data)
        data_in.iloc[:, 1] = data
        print(data_in)

    def smmoth(self):
        self.mysmooth(self.data_short)
        self.mysmooth(self.data_long)

    def mysmooth(self, data_in):
        data = data_in.iloc[:, 1]
        # ref, https://www.cnblogs.com/nxf-rabbit75/p/10669516.html
        # or, https://blog.csdn.net/Einsam0/article/details/81772230
        data = data.rolling(5).mean()
        print(data)
        data_in.iloc[:, 1] = data
        print(data_in)

    def calc_diff(self):
        self.data_diff = self.data_short.copy()
        self.data_diff.iloc[:, 0] = self.data_short.iloc[:, 0]
        self.data_diff.iloc[:, 1] = self.data_short.iloc[:, 1] - \
            self.data_long.iloc[:, 1]
        # calculate_min_dose_differe

    def run_all(self, fn_exel, fn_excel2):
        self.parse_exel(fn_exel)
        self.parse_exel2(fn_excel2)
        self.smmoth()
        self.myinterp()
        self.nomalization()
        self.calc_diff()
        self.plot_gamma1()
        self.calc_gamma()
        self.plot_gamma2()
        self.plot_gamma3()
        self.plot_gamma4()

    def set_pos_range(self, pos):
        self.pos_range = pos


if __name__ == "__main__":
    fn_excel2 = "gamma_Index20210828.xlsx"
    gip = GammaIndexPlot()
    fn_exel = "gamma_new_20210821.xlsx"
    gip.set_pos_range((0, 30))
    gip.run_all(fn_exel, fn_excel2)

    gip1 = GammaIndexPlot()
    fn_exel = "gamma_new_20210822.xlsx"
    gip1.set_pos_range((-30, 30))
    gip1.run_all(fn_exel, fn_excel2)

    gip2 = GammaIndexPlot()
    fn_exel = "gamma_new_20210822_new.xlsx"
    gip2.set_pos_range((-30, 30))
    gip2.run_all(fn_exel, fn_excel2)
