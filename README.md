# gamma_pass_rate_plot

#### 需求
##### 需求效果
![短数据剂量图](pic/xuqiu.bmp)

##### 具体如下
1. 根据两条pdd曲线(第一条为PRIMO模拟结果，第二条为测量结果，画出gamma分析结果Dose(%)、Gamma Index、Percentage of points，伽马分析阈值为3mm/3%；
2. 数据插值，然后相减，求difference曲线，原话为“Difference就是归一化后的Y1-Y2的值”
3. 绘图背景要求为白色，使用Matlab、Python、octave、ROOT等作为绘图工具；
4. 第一幅图，绘图背景要求为白色：为双Y轴：X轴Position（cm）：范围（0cm-30cm），第一个纵坐标：Dose（%）范围：0%-100%；第二个：Y轴：Difference（%）范围-20%-20%，数据间隔为原始数据间隔；Legend为 calculated、Measured、Difference，使用三种不同的颜色；
5. 第二幅图：绘图背景要求为白色X轴Position（cm）范围（0cm-30cm），Y轴Gamma Index 范围为0-1.2，数据间隔为原始数据间隔;
6. 第三幅图：绘图背景要求为白色，X轴坐标Gamma Index范围（0-4），Y轴Percentage of points范围0-50.2;数据间隔为原始数据间隔;
7. 解读Excel文档：
    1. X1范围为A1:A175，数据范围为0:0.2:34.8;Y1范围为B1:B175, 为PRIMO模拟得到的PDD纵坐标
    2. X2范围为D1:D301,数据范围为0:0.1:30;Y2范围E1:E301，为测量得到的PDD纵坐标值；
8. Y值需要单独归一化
9. 去掉顶部横线
10. .绘制直方图，横坐标为Gamma Index,纵坐标为Percentage of points,横坐标范围为0到4，刻度为1；纵坐标范围0-100，刻度为10，要求输出图片为png格式，分辨率为800x200

#### 流程图
![](pic/flow_chart_pass_rate_plot.png)

#### 效果
<!-- ![短数据剂量图](fig_short.png) -->


###### gamma_new_20210821
![](pic/gamma_new_20210821_fig_long.png)

![](pic/gamma_new_20210821_gamma_index.png)

![](pic/gamma_new_20210821_gamma_passrate.png)

![](pic/gamma_new_20210821_gamma_passrate_fake.png)

###### gamma_new_20210822
![](pic/gamma_new_20210822_fig_long.png)

![](pic/gamma_new_20210822_gamma_index.png)

![](pic/gamma_new_20210822_gamma_passrate.png)

![](pic/gamma_new_20210822_gamma_passrate_fake.png)

###### gamma_new_20210822_new
![](pic/gamma_new_20210822_new_fig_long.png)

![](pic/gamma_new_20210822_new_gamma_index.png)

![](pic/gamma_new_20210822_new_gamma_passrate.png)

![](pic/gamma_new_20210822_new_gamma_passrate_fake.png)

<!-- ![](fig_all.png) -->
